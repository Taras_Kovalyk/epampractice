﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {

            //init rectangle with int values of diagonal points
            Rectangle first = new Rectangle(1, 2, 4, 5);

            //init rectangle with diagonal points
            Point A;
            A.X = 0; A.Y = 0;
            Point C;
            C.X = 3;C.Y = 4;
            Rectangle second = new Rectangle(A,C);

            Console.WriteLine("First rectangle:\n {0}",first.ToString() );
            Console.WriteLine("Second rectangle:\n {0}", second.ToString());

            Console.WriteLine("\n\t\t Class working demo\n\n");
            #region intersection
            // the result of intersection of first and second rectangle
            var intersection = Rectangle.Intersect(first, second);
            Console.WriteLine("Intersection of first and second rectangle:\n {0} ", intersection.ToString());
            #endregion

            #region changing size
            Console.WriteLine("\nChanging size of a first rectangle A point is stable\n");
            first.ChangeSizeStable(2, 3);

            Console.WriteLine("First rectangle:\n {0}", first.ToString());

            Console.WriteLine("\nChanging size of a second rectangle proportionally\n");
            second.ChangeSizeProp(2, 3);

            Console.WriteLine("Second rectangle:\n {0}", second.ToString());
            #endregion

            #region Containing rectangle

            var containingRectangle = Rectangle.ContainingRect(first, second);

            Console.WriteLine("\nCalculating rectangle than contains both first and second rectangle\n");
            Console.WriteLine("First rectangle:\n {0}", first.ToString());
            Console.WriteLine("Second rectangle:\n {0}", second.ToString());
            Console.WriteLine("\nMinimal rectangle that contains both rectangles\n {0}", containingRectangle.ToString());

            #endregion

            #region changing position
            Console.WriteLine("Moving rectangle on a vector\n");

            Console.WriteLine("First rectangle:\n {0}", first.ToString());
            first.MoveRectengle(2, 2);
            Console.WriteLine("Moved first rectangle on vector (2,2):\n {0}", first.ToString());
            #endregion

            Console.ReadKey();
        }

        
    }
}
