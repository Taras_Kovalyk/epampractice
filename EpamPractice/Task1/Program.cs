﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int firstIndex = -3;
            int lenght = 5;
            MyArray firstArr = new MyArray(firstIndex, lenght);
            MyArray secondArr = new MyArray() { FirstIndex = firstIndex, Lenght = lenght };

            Random rand = new Random();
            for (int i = firstIndex; i < firstIndex + lenght; i++)
            {
                firstArr[i] = rand.Next(-100, 100);
                secondArr[i] = rand.Next(-10, 100);
            }

            Console.WriteLine("First array: \n{0}",firstArr.ToString());
            Console.WriteLine("Second array: \n{0}",secondArr.ToString());

            Console.WriteLine("\t\tClass working demo\n");

            Console.WriteLine("First array is multiplied on scalar(-1)\n");
            firstArr.MultiplyOnScalar(-1);
            Console.WriteLine("Result array\n");
            Console.WriteLine(firstArr.ToString());

            Console.WriteLine("Adding second array to first\n");
            Console.WriteLine("First array: \n{0}", firstArr.ToString());
            Console.WriteLine("Second array: \n{0}", secondArr.ToString());
            firstArr.AddArrays(secondArr);
            Console.WriteLine("Result array\n");
            Console.WriteLine(firstArr.ToString());

            Console.WriteLine("\nSubtract first array from second\n");
            Console.WriteLine("First array: \n{0}", firstArr.ToString());
            Console.WriteLine("Second array: \n{0}", secondArr.ToString());
            secondArr.Substract(firstArr);
            Console.WriteLine("Result array\n");
            Console.WriteLine(secondArr.ToString());

            var thirdArr = MyArray.AddArrays(firstArr, secondArr);
            Console.WriteLine("\nThird array: \n{0}", thirdArr.ToString());

            Console.WriteLine("Comparing third to first array\n");

            Console.WriteLine("Result: " + thirdArr.SequenceEquals(firstArr).ToString());
            thirdArr = firstArr;
            Console.WriteLine("Compare after assigning first to third");
            Console.WriteLine("Result: " + thirdArr.SequenceEquals(firstArr).ToString());

            Console.ReadKey();
        }
    }
}
